package hw.pkg2.pkg25;

import java.util.Scanner;

public class HW225 {

      public static void main(String[] args) {
      // the intergers in line 10 will be used to gatehr user input. 
      int x=0;
      // line 11 allows for the program to gather input from the keyboard.
      Scanner input = new Scanner(System.in);
      
      // lines 14-15 displayes a message to the user to get input
      System.out.print("Please enter a number to determine if it is an even "
              + "number or an odd number: ");
      // line 17 will store the data that the user inputed
      x = input.nextInt();
      
      // lines 21-24 will check to is if the number entered form the user is 
      // even and display a message 
      if (x%2==0)
      {
          System.out.println("The number " + x + " is an even number");
      }
      // lines 27-30 will check to is if the number entered form the user is 
      // odd and display a message
      else
      {
          System.out.println("The number " + x + " is an odd number");
      }
    }
    
}
