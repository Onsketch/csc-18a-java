package hw.pkg2.pkg26;

import java.util.Scanner;

public class HW226 {


    public static void main(String[] args) {
       // lines 10-11 are used to gather data from user
        int x=0;
        int y=0;
        // line 13 allows data from the keyboard to be gathered
        Scanner input = new Scanner(System.in);
        
        // line 16-17 tells the user what this program is about 
        System.out.println("what to find out if a number is a Multiple of "
                + " another? ");
        // lines 19-20 ask the user a question then allows for a response
        System.out.print("Enter First number: ");
        x = input.nextInt();
        // lines 22-23 ask the user a question then allows for a response
        System.out.print("Enter a second number: ");
        y = input.nextInt();
        
        // lines 27-30 checks to see if x is a multiple of y and then displays
        // a message
        if (y % x == 0) 
        {
         System.out.println(x +" and " + y + " are multiples");
        }
        // lines 33-36 checks to see if x is not a multiple of y and then 
        // displays a message
         else
        {
         System.out.println(x +" and " + y + " are not multiples"); 
        }
    }
    
}
