
package classexample;


public class ClassExample {

    
    public static void main(String[] args) {
       
      System.out.println("Examples of %d format specifier: ");
      System.out.println("");
      
      // this set of codes makes things left justified
      String my_format_string = "%8d%n";
      
      System.out.printf(my_format_string,34);
      System.out.printf(my_format_string,384);
      System.out.printf(my_format_string,221);
      System.out.printf(my_format_string,1026);
      System.out.printf(my_format_string,16384);
      System.out.println("");
      
      // this set of codes makes everything right justified
      String my_format_string2 = "%-8d\n";
      
      System.out.printf(my_format_string2,134);
      System.out.printf(my_format_string2,1384);
      System.out.printf(my_format_string2,1221);
      System.out.printf(my_format_string2,11026);
      System.out.printf(my_format_string2,116384);
      System.out.println("");
      
      // this set of code makes things left justified and
      // it makes the front numbers show a zero
      String my_format_string3 = "%08d%n";
      
      System.out.printf(my_format_string3,34);
      System.out.printf(my_format_string3,384);
      System.out.printf(my_format_string3,221);
      System.out.printf(my_format_string3,1026);
      System.out.printf(my_format_string3,16384);
      System.out.println("");
      
      // this set of codes makes it right justified
      // and it also makes it a point vaule with repeating
      // zeros at the end
      String my_format_string4 = "%f%n";
      
      System.out.printf(my_format_string4,3.4);
      System.out.printf(my_format_string4,3.84);
      System.out.printf(my_format_string4,22.17);
      System.out.printf(my_format_string4,10.26);
      System.out.printf(my_format_string4,163.84);
      System.out.println("");
      
      // this set of codes makes it set at a 2 decimal point
      // value
      String my_format_string5 = "%2.2f%n";
      
      System.out.printf(my_format_string5,3.4);
      System.out.printf(my_format_string5,3.84);
      System.out.printf(my_format_string5,22.17);
      System.out.printf(my_format_string5,10.26);
      System.out.printf(my_format_string5,163.84);
      System.out.println("");
      
      // this makes it so it has a decimal value as well as 
      // 3 digits behind the decimal
      String my_format_string6 = "%.3f%n";
      
      System.out.printf(my_format_string6,3.4);
      System.out.printf(my_format_string6,3.84);
      System.out.printf(my_format_string6,22.17);
      System.out.printf(my_format_string6,10.26);
      System.out.printf(my_format_string6,163.84);
      System.out.println("");
    }
    
}
