package arithmeticwithscanner;

import java.util.Scanner; 

public class ArithmeticwithScanner {


    public static void main(String[] args) {
        int x,y,sum,difference,product, quantity;
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("enter an interger vaule for x: ");
        x = keyboard.nextInt(); 
        
        System.out.println("enter an interger vaule for y: ");
        y = keyboard.nextInt(); 
        
        sum = (x+y);
        difference = (x-y);
        product = (x*y);
        quantity = (x/y);
        
        System.out.println("the sum of " + x +" and " + y + " is " 
        + sum);
        
        System.out.println("the difference of " + x+ " and " +y+ " is " 
        +difference);
        
        System.out.println("the product of " +x+ " and " +y+ " is " 
        +product);
        
        System.out.println("the quantity of " +x+ " and " +y+ " is " 
        +quantity);
        
        System.out.println("another way to do is: ");
        System.out.println("%d + %d = %d%n ",x,y,sum);
        System.out.println("%d - %d = %d%n ",x,y,difference);
        System.out.println("%d * %d = %d%n ",x,y,product);
        System.out.println("%d / %d = %d%n ",x,y,quantity);
    }
    
}
